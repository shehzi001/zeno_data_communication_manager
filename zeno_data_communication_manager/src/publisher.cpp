/*
 * Copyright [2016] <Bonn-Rhein-Sieg University>
 *
 * publisher.cpp
 *
 *  Created on: Mar 29, 2016
 *      Author: Shehzad Ahmed
 */

#include <zeno_data_communication_manager/publisher.h>

using namespace DataCommunicationManager;

Publisher::Publisher(Node &node_handle, const std::string &topic_name, 
                        const std::string &topic_type, double queue_size)
{
    std::string topic_name_temp = topic_name;

    std::size_t found = topic_name_temp.find('~');

    if (found!=std::string::npos and found == 0) {
        topic_name_temp.replace(found, 1, '/' + node_handle.getNodeName() + '/');
    }

    std::cout << "["  << node_handle.getNodeName() << "] Publisher topic " << topic_name_temp << " is registered"<< std::endl;

    Address address_pub(topic_type);
    address_pub.setSubject(topic_name_temp);

    Session session = node_handle.getSessionHandle();

    try {
            sender_ = session.createSender(address_pub);
            sender_.setCapacity(queue_size);

    } catch(const std::exception& error) {
            std::cerr << error.what() << std::endl;
    }
}

Publisher::~Publisher()
{
    sender_.close();
}


bool Publisher::publish(const qpid::messaging::Message msg)
{
    try {
            sender_.send(msg);
            sender_.getSession().acknowledge();
            return true;

    } catch(const std::exception& error) {
            std::cerr << error.what() << std::endl;
            return false;
    }
}