# Important usage Instruction
This repository provide a simple communication manager on top of Qpid library to create nodes in C++ and Python
and pass messages using publisher-subscriber model.

# Pre-requisite installations:
  * qpid (download qpid-broker-0.16 and extract to e.g. /home/user_name/qpid-broker-0.16)
  * Java 1.6 or 1.7
  * Boost 1.45.0
  * CMake 2.8.7

# Clone repository for source installation

    git clone git@bitbucket.org:shehzi001/zeno_data_communication_manager.git

# Installation

    cd zeno_data_communication_manager/zeno_data_communication_manager/ 
    mkdir build && cd  build
    cmake ..
    make
    sudo make install

# Test installation

    cd zeno_data_communication_manager/zeno_dcm_test
    mkdir build && cd build
    cmake ..
    make

## Running qpid broker

    Open new terminal:
    >> qpid-server
    Note: qpid-server will act as a server and listen on a specific port
          e.g. 5672.

## Examples(Python)

    Open new terminal:
    cd zeno_data_communication_manager/zeno_dcm_test/scripts/
    >> ./publisher_test

    Open new terminal:
    cd zeno_data_communication_manager/zeno_dcm_test/scripts/
    >> ./subsriber_test

## Examples(cpp)

    Open new terminal:
    cd zeno_data_communication_manager/zeno_dcm_test/build/
    >> ./pub

    Open new terminal:
    cd zeno_data_communication_manager/zeno_dcm_test/build/
    >> ./sub
