#!/usr/bin/env python
'''
An example of creating node and publisher/subsriber.

Author: Shehzad Ahmed
Date: 29.03.2016
'''

import time
# Import the modules we need
from qpid.messaging import *
#from data_communication_manager import *
from zeno_data_communication_manager.data_communication_manager import *


def joint_states_cb(msg):
    print msg

def hello_world_cb(msg):
    print msg


def hello_world_2_cb(msg):
    print msg
 
if __name__ == "__main__":
    broker     = "tcp://localhost:5672"
    connection_options = {'sasl_mechanisms':'ANONYMOUS PLAIN MD5',
                          'username':'admin',
                          'password':'admin'}
    zeno_py = Node()
    zeno_py.init_node("joint_controller_sub", broker, connection_options, True)
    zeno_py.set_loop_time(0.01)
    sub_1 = Subscriber(zeno_py, "/joint_controller/joint_states", "amq.topic", 1, joint_states_cb)
    #pub = Publisher(zeno_py, "~hello_world", "amq.topic", 1)
    #pub_2 = Publisher(zeno_py, "~hello_world_2", "amq.topic", 1)
    
    #sub_2 = Subscriber(zeno_py, "joint_states", "amq.topic", 1, hello_world_cb)
    sub_3 = Subscriber(zeno_py, "hello_world", "amq.topic", 1, hello_world_2_cb)
    while zeno_py.is_shutdown():
        #pub.publish("hello world")
        #pub_2.publish("hello_world_2")
        time.sleep(2)

    zeno_py.shutdown()
