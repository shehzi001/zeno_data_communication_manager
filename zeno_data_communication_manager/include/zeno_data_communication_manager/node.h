/*
 * Copyright [2016] <Bonn-Rhein-Sieg University>
 *
 * publisher.cpp
 *
 *  Created on: Mar 29, 2016
 *      Author: Shehzad Ahmed
 */
 
#ifndef DATA_COMMUNICATION_MANAGER_NODE_H
#define DATA_COMMUNICATION_MANAGER_NODE_H

#include <qpid/messaging/Connection.h>
#include <qpid/messaging/Message.h>
#include <qpid/messaging/Receiver.h>
#include <qpid/messaging/Sender.h>
#include <qpid/messaging/Session.h>
#include <qpid/messaging/Address.h>
#include <qpid/types/Variant.h>
#include <boost/shared_ptr.hpp>

#include <iostream>
#include <string>
#include <vector>

using namespace qpid::messaging;
using namespace qpid::types;

/**
 * @brief The Data Communication Manager.
 */
namespace DataCommunicationManager
{
    /**
     * @class A Node for Data Communication Management.
     */
    class Node
    {
        public:
            /**
             * A constructor.
             */
            explicit Node();

            /**
             * A destructor.
             */
            virtual ~Node();

            /**
             * @brief Initilized Node.
             * @param node_name node name.
             * @param qpid_broker_address address to connect with qpid server
             *                            e.g 'localhost:11311'
             * @param connection_options  e.g. "{sasl_mechanisms:PLAIN,
             *                                   username:guest,
             *                                   password:guest}"
             * @param reconnect flag to specify reconnect option for the qpid connection.
             */
            bool initNode(const std::string &node_name, const std::string &qpid_broker_address,
                              const std::string &connection_options, bool reconnect);

            Session getSessionHandle() {
                return session_handle_;
            }

            void setLoopTime(double loop_time) {
                loop_time_ = loop_time;
            }

            double getLoopTime() {
                return loop_time_;
            }

            std::string getNodeName() {
                return node_name_;
            }

            bool isNodeRunning() {
                return connection_->isOpen();
            }

            bool shutdown();

        private:
            /**
             * Copy Ctor.
             */
            Node(const Node &other);

            /**
             * Assignment operator
             */
            Node &operator=(const Node &other);


        private:
            /**
             * Stores node name
             */
            std::string node_name_;

            /**
             * Stores broker address
             */
            std::string broker_;

            /**
             * The session interface with the qpid broker.
             */
            Session session_handle_;


            /**
             * stores loop time in secs
             */
            double loop_time_;

            /**
            * The connection interface to the qpid broker.
            */
            boost::shared_ptr<Connection> connection_;
    };

}  // namespace data_communication_manager
#endif  // DATA_COMMUNICATION_MANAGER_NODE_H


/*
std::string broker = "localhost:5672";
    std::string address =  "amq.topic";
    std::string connectionOptions =  "{sasl_mechanisms:PLAIN,username:guest,password:guest}";
    Sender sender;
    

    Address address_pub("amq.topic");
    address_pub.setSubject("joint_states");
    //address_pub.setName ("joint_controller");

    try {
        connection.reset(new Connection(broker, connectionOptions));
        connection->setOption("reconnect", true);
        connection->open();
        Session session = connection->createSession();
        sender = session.createSender(address_pub);
        sender.setCapacity(1);
        } catch(const std::exception& error) {
        std::cerr << error.what() << std::endl;
        return 1;
    }
*/