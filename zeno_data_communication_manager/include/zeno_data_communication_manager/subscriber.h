/*
 * Copyright [2016] <Bonn-Rhein-Sieg University>
 *
 * publisher.cpp
 *
 *  Created on: Mar 29, 2016
 *      Author: Shehzad Ahmed
 */

#ifndef DATA_COMMUNICATION_MANAGER_SUBSCRIBER_H_
#define DATA_COMMUNICATION_MANAGER_SUBSCRIBER_H_

#include <zeno_data_communication_manager/node.h>

#include <qpid/messaging/Connection.h>
#include <qpid/messaging/Message.h>
#include <qpid/messaging/Receiver.h>
#include <qpid/messaging/Sender.h>
#include <qpid/messaging/Session.h>
#include <qpid/messaging/Address.h>
#include <qpid/types/Variant.h>

#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>
#include <boost/thread.hpp>

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <list>

using namespace qpid::messaging;
using namespace qpid::types;

/**
 * @brief The Data Communication Manager.
 */
namespace DataCommunicationManager
{
    /**
     * @class A Subscriber for Data Communication Management.
     */
    class Subscriber
    {
        public:
            /**
             * A constructor.
             */
            Subscriber(DataCommunicationManager::Node &node_handle, const std::string &topic_name, 
                        //const std::string &topic_type, void (*function)(qpid::messaging::Message), double queue_size=10);
                        const std::string &topic_type, boost::function< void(qpid::messaging::Message) > f, double queue_size);
            /**
             * A destructor.
             */
            virtual ~Subscriber();

            /**
             * @brief Starts a subscription thread and listens for incoming messages.
             * @msg prepares and publishes list messages.
             */
            //void subscribe(void (*function)(qpid::messaging::Message));
             void subscribe(boost::function< void(qpid::messaging::Message) > f);


            /**
             * @brief Shutdown the subscriber topic
             */
            bool shutdown();

        private:
            /**
             * Copy Ctor.
             */
            Subscriber(const Subscriber &other);

            /**
             * Assignment operator
             */
            Subscriber &operator=(const Subscriber &other);

        private:
            /**
             * Stores Subscriber name
             */
            std::string subscriber_name_;

            /**
             * The session interface with the qpid broker.
             */
            Receiver receiver_;

            /**
             *
             */
            double loop_time_;
    };

}  // namespace data_communication_manager
#endif  // DATA_COMMUNICATION_MANAGER_SUBSCRIBER_H_
