#!/usr/bin/env python
'''
This module provides functionality to create 
node, publisher and subsriber using qpid communication.
Author: Shehzad Ahmed
Date: 29.03.2016
'''

import os
import time
from qpid.messaging import *
from thread import start_new_thread, exit_thread
from zeno_data_communication_manager.DCMException import *
import time

class Node():
    def __init__(self):
        self.connection = None
        self.broker = None
        self.is_running = False
        self.node_name = None 
        self.session = None
        self.loop_time = 0.1

    def init_node(self, node_name = None, qpid_broker_address = None,
                  connection_options = None, reconnect = False):
        self.broker = qpid_broker_address
        self.connection = Connection(self.broker)
        self.connection.reconnect = reconnect
        if connection_options:
            self.connection.sasl_mechanisms = connection_options['sasl_mechanisms'] #'PLAIN'
            self.connection.username = connection_options['username'] #'guest'
            self.connection.password = connection_options['password'] #'guest'
        self.node_name = node_name

        try:
            self.connection.open()
            # Define the session
            self.session = self.connection.session()

            if not self.session.closed:
                self.is_running = True
                print ("[{0}] Node is now running.".format(node_name))
            else:
                 self.is_running = False
                 raise DCMException('[{0}] Unable to connect with the qpid broker.'.format(node_name))
        except DCMException as err:
            self.is_running = False
            self.connection.close()
            print('[{0}] {1}'.format(self.node_name, err.value))
            raise
        except MessagingError, err:
            self.is_running = False
            self.connection.close()
            print('[{0}] {1}'.format(self.node_name, err))
            raise

        return self.is_running

    def get_node_name(self):
        return self.node_name

    def is_shutdown(self):
        return self.session.closed

    def set_loop_time(self, loop_time):
        self.loop_time = loop_time

    def shutdown(self):
        try:
            self.is_running = False
            self.connection.close()
            self.session.close()
        except:
            pass

    def __del__(self):
        print('[Zeno DCM]: Killing on exit')
        #self.shutdown()  

class Publisher():
    def __init__(self, node_handle=None, topic_name=None, topic_type="amq.topic", queue_size=10):

        if node_handle is None:
            print('Node is not initilized.')

        self.node_handle = node_handle

        if topic_name.find("~") == 0:
            self.topic_name = topic_name.replace('~',str('/') + self.node_handle.node_name + str('/'))
        else:
            self.topic_name = topic_name

        self.publisher = self.node_handle.session.sender(topic_type)
        self.publisher.capacity = queue_size

        print ("[{0}] Publisher topic {1} is registered.".format(self.node_handle.node_name, self.topic_name))

    def publish(self, msg):
        if self.node_handle.is_shutdown():
            return
        try:
            msg = Message(content=msg)
            msg.subject = self.topic_name
            self.publisher.send(msg);
            #self.node_handle.session.acknowledge()
        except MessagingError, err:
            #print('[{0}] {1}'.format(self.topic_name, err))
            pass
        except:
            pass

    def __del__(self):
        pass

class Subscriber():
    def __init__(self, node_handle=None, topic_name=None, topic_type="amq.topic", queue_size=10, call_back=None):

        if node_handle is None:
            print('Node is not initilized.')

        self.node_handle = node_handle

        if call_back is None:
             print('[{0}] call back is of Nonetype.'.format(self.node_handle.node_name))
             return

        if topic_name.find("~") == 0:
            self.topic_name = topic_name.replace('~',str('/') + self.node_handle.node_name + str('/'))
        else:
            self.topic_name = topic_name

        self.subscriber = self.node_handle.session.receiver(topic_type)

        self.subscriber.capacity = queue_size

        print ("[{0}] Subscribed to topic {1}.".format(self.node_handle.node_name, self.topic_name))

        self.subscriber_thread = start_new_thread(self.subscribe, (call_back, ))

    def subscribe(self, call_back=None):

        while(not self.node_handle.is_shutdown()):
            try:
                msg = None
                msg = self.subscriber.fetch()
                parsed_data = None
                if msg.subject == self.topic_name:
                    parsed_data = self.parse_message(msg)

                #self.node_handle.session.acknowledge()
                if parsed_data is not None:
                    call_back(parsed_data)
            except exceptions.Empty, err:
                #print('[{0}] {1}'.format(self.topic_name, err))
                pass
            except exceptions.MessagingError, err:
                print('[{0}] {1}'.format(self.topic_name, err))
            except exceptions.ReceiverError, err:
                print('[{0}] {1}'.format(self.topic_name, err))
            except:
                print('[{0}] topic is unsubscribed due to unknown error'.format(self.topic_name))
                break

    def parse_message(self, msg):
        try:
            return msg.content
        except:
            print('[{0}] Topic does not contain content field.'.format(self.node_name, err))
            return None

    def __del__(self):
        try:
            exit_thread()
        except:
            pass

