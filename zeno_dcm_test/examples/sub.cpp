/*
 * Copyright [2016] <Bonn-Rhein-Sieg University>
 *
 * publisher.cpp
 *
 *  Created on: Mar 29, 2016
 *      Author: Shehzad Ahmed
 */
 
#include <qpid/messaging/Message.h>
#include <qpid/types/Variant.h>
#include <iostream>

#include <zeno_data_communication_manager/node.h>
#include <zeno_data_communication_manager/subscriber.h>

using namespace qpid::messaging;
using namespace qpid::types;

void callbackFunc(qpid::messaging::Message message)
{
    std::cout << "message rec: " << std::endl;
    Variant::Map joint_states;
    decode(message, joint_states);
    Variant::List joint_names = joint_states["joint_names"].asList();
    Variant::List joint_values = joint_states["joint_positions"].asList();

    if(!joint_names.empty() and !joint_values.empty()) {
        std::cout << joint_names << std::endl;
        std::cout << joint_values << std::endl;
    }
}

int main(int argc, char** argv) {

    double cycle_time = 1.0;
    
    std::string broker = "localhost:5672";

    std::string topic_type =  "amq.topic";

    std::string connectionOptions =  "{sasl_mechanisms:PLAIN,username:admin,password:admin}";

    DataCommunicationManager::Node node_handle;

    node_handle.initNode("joint_controller_sub", broker, connectionOptions, true);

    node_handle.setLoopTime(cycle_time);

    DataCommunicationManager::Subscriber sub(node_handle, "/joint_controller/joint_states", 
                                            topic_type, &callbackFunc , 1);

    while(node_handle.isNodeRunning()) {
        sleep(cycle_time);
    }

    return 0;
}
