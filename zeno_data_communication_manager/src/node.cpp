/*
 * Copyright [2016] <Bonn-Rhein-Sieg University>
 *
 * publisher.cpp
 *
 *  Created on: Mar 29, 2016
 *      Author: Shehzad Ahmed
 */
 
#include <zeno_data_communication_manager/node.h>

using namespace DataCommunicationManager;


Node::Node():
    loop_time_(0.1)
{
}

Node::~Node()
{
    shutdown();
}

bool Node::initNode(const std::string &node_name, const std::string &qpid_broker_address,
                     const std::string &connection_options, bool reconnect=false)
{
    node_name_ = node_name;
    broker_ = qpid_broker_address;
    //std::string connectionOptions =  "{sasl_mechanisms:PLAIN, username:guest,password:guest}";

    try {

        connection_.reset(new Connection(broker_, connection_options));
        connection_->setOption("reconnect", reconnect);
        connection_->open();
        session_handle_ = connection_->createSession();
    } catch(const std::exception& error) {

        std::cerr << error.what() << std::endl;
        return false;
    }
    return true;
}

bool Node::shutdown() {
    if (!connection_->isOpen())
        return true;
    connection_->close();
    return true;
}
