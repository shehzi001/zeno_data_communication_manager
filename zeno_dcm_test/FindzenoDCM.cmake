# - Try to find zenoDCM
# Once done this will define
#
#  zenoDCM_FOUND - system has zenoDCM
#  zenoDCM_INCLUDE_DIRS - the zenoDCM include directory
#  zenoDCM_LIBRARIES - Link these to use zenoDCM
#  zenoDCM_DEFINITIONS - Compiler switches required for using zenoDCM
#
#  Copyright (c) 2011 Lee Hambley <lee.hambley@gmail.com>
#
#  Redistribution and use is allowed according to the terms of the New
#  BSD license.
#  For details see the accompanying COPYING-CMAKE-SCRIPTS file.
#

if (zenoDCM_LIBRARIES AND zenoDCM_INCLUDE_DIRS)
  # in cache already
  set(zenoDCM_FOUND TRUE)
else (zenoDCM_LIBRARIES AND zenoDCM_INCLUDE_DIRS)

  find_library(zenoDCM_LIBRARY
    NAMES
      zenoDCM
    PATHS
      /usr/lib/zeno_data_communication_manager/
  )

  set(zenoDCM_INCLUDE_DIRS
    /usr/include/
  )

  if (zenoDCM_LIBRARY)
    set(zenoDCM_LIBRARIES
        ${zenoDCM_LIBRARIES}
        ${zenoDCM_LIBRARY}
    )
  endif (zenoDCM_LIBRARY)

  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(zenoDCM DEFAULT_MSG zenoDCM_LIBRARIES zenoDCM_INCLUDE_DIRS)

  # show the zenoDCM_INCLUDE_DIRS and zenoDCM_LIBRARIES variables only in the advanced view
  mark_as_advanced(zenoDCM_INCLUDE_DIRS zenoDCM_LIBRARIES)

endif (zenoDCM_LIBRARIES AND zenoDCM_INCLUDE_DIRS)

