#!/usr/bin/env python

'''
This module provides functionality to create 
node, publisher and subsriber using qpid communication.
Author: Shehzad Ahmed
Date: 29.03.2016
'''

class DCMException(Exception):
    def __init__(self, value):
         self.value = value
    def __str__(self):
         return repr(self.value)