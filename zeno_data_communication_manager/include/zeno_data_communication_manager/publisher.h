/*
 * Copyright [2016] <Bonn-Rhein-Sieg University>
 *
 * publisher.cpp
 *
 *  Created on: Mar 29, 2016
 *      Author: Shehzad Ahmed
 */
 
#ifndef DATA_COMMUNICATION_MANAGER_PUBLISHER_H_
#define DATA_COMMUNICATION_MANAGER_PUBLISHER_H_

#include <zeno_data_communication_manager/node.h>

#include <qpid/messaging/Message.h>
#include <qpid/messaging/Sender.h>
#include <qpid/messaging/Session.h>
#include <qpid/messaging/Address.h>
#include <qpid/types/Variant.h>

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <list>

using namespace qpid::messaging;
using namespace qpid::types;

/**
 * @brief The Data Communication Manager.
 */
namespace DataCommunicationManager
{
    /**
     * @class A Publisher for Data Communication Management.
     */
    class Publisher
    {
        public:
            /**
             * A constructor.
             */
            explicit Publisher(Node &node_handle, const std::string &topic_name, 
                        const std::string &topic_type, double queue_size=10);

            /**
             * A destructor.
             */
            virtual ~Publisher();

            /**
             * @brief Initilized planner interface.
             * @msg prepares and publishes list messages.
             */
            virtual bool publish(const qpid::messaging::Message msg);


        private:
            /**
             * Stores Publisher name
             */
            std::string Publisher_name_;

            /**
             * The session interface with the qpid broker.
             */
            Sender sender_;
    };

}  // namespace data_communication_manager
#endif  // DATA_COMMUNICATION_MANAGER_PUBLISHER_H_
