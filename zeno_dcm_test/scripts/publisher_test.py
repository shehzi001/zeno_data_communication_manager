#!/usr/bin/env python

'''
An example of creating node and publisher.

Author: Shehzad Ahmed
Date: 29.03.2016
'''

import time
# Import the modules we need
from qpid.messaging import *
from zeno_data_communication_manager.data_communication_manager import *

def joint_states_cb(msg):
    print msg

def hello_world_cb(msg):
    print msg
 
if __name__ == "__main__":
    broker = "tcp://localhost:5672"
    connection_options = {'sasl_mechanisms':'ANONYMOUS PLAIN MD5',
                          'username':'admin',
                          'password':'admin'}
    zeno_py = Node()
    zeno_py.init_node("joint_command_pub", broker,
                      connection_options=connection_options)
    #sub_1 = Subscriber("joint_states", "amq.topic", 1, joint_states_cb)
    pub = Publisher(zeno_py, "/joint_controller/position_commands", "amq.topic", 1)
    joint_names = ['left_shoulder_pitch', 'left_shoulder_roll', 'left_elbow_yaw', 'left_elbow_pitch']
    joint_values = [0.0, 1.0, 2.0, 3.0]

    msg = {
            'joint_names': joint_names,
            'joint_positions': joint_values
          }

    while True:
        pub.publish(msg)
        time.sleep(1)

    zeno_py.shutdown()
