# - Try to find qpid
# Once done this will define
#
#  qpid_FOUND - system has qpid
#  qpid_INCLUDE_DIRS - the qpid include directory
#  qpid_LIBRARIES - Link these to use qpid
#  qpid_DEFINITIONS - Compiler switches required for using qpid
#
#  Copyright (c) 2011 Lee Hambley <lee.hambley@gmail.com>
#
#  Redistribution and use is allowed according to the terms of the New
#  BSD license.
#  For details see the accompanying COPYING-CMAKE-SCRIPTS file.
#

if (qpid_LIBRARIES AND qpid_INCLUDE_DIRS)
  # in cache already
  set(qpid_FOUND TRUE)
else (qpid_LIBRARIES AND qpid_INCLUDE_DIRS)

  find_library(qpid_LIBRARY_MESSAGING
    NAMES
      qpidmessaging 
    PATHS
      /usr/lib
      /usr/local/lib
      /opt/local/lib
      /sw/lib
  )

  find_library(qpid_LIBRARY_TYPES
    NAMES
      qpidtypes
    PATHS
      /usr/lib
      /usr/local/lib
      /opt/local/lib
      /sw/lib
  )

  set(qpid_INCLUDE_DIRS
    /usr/local/include
  )

  if (qpid_LIBRARY_MESSAGING AND qpid_LIBRARY_TYPES)
    set(qpid_LIBRARIES
        ${qpid_LIBRARIES}
        ${qpid_LIBRARY_MESSAGING} ${qpid_LIBRARY_TYPES}
    )
  endif (qpid_LIBRARY_MESSAGING AND qpid_LIBRARY_TYPES)

  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(qpid DEFAULT_MSG qpid_LIBRARIES qpid_INCLUDE_DIRS)

  # show the qpid_INCLUDE_DIRS and qpid_LIBRARIES variables only in the advanced view
  mark_as_advanced(qpid_INCLUDE_DIRS qpid_LIBRARIES)

endif (qpid_LIBRARIES AND qpid_INCLUDE_DIRS)

