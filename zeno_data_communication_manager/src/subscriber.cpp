/*
 * Copyright [2016] <Bonn-Rhein-Sieg University>
 *
 * subscriber.cpp
 *
 *  Created on: Mar 29, 2016
 *      Author: Shehzad Ahmed
 */

#include <zeno_data_communication_manager/subscriber.h>

using namespace DataCommunicationManager;

Subscriber::Subscriber(DataCommunicationManager::Node &node_handle, const std::string &topic_name, 
                        const std::string &topic_type, boost::function< void(qpid::messaging::Message) > f,
                        double queue_size)
{
    std::string topic_name_temp = topic_name;

    std::size_t found = topic_name_temp.find('~');

    if (found!=std::string::npos and found == 0) {
        topic_name_temp.replace(found, 1, '/' + node_handle.getNodeName() + '/');
    }

    std::cout << "["  << node_handle.getNodeName() << "] Subscribed to topic " << topic_name_temp << std::endl;

    Address address_pub(topic_type);
    address_pub.setSubject(topic_name_temp);

    Session session = node_handle.getSessionHandle();

    try {
            receiver_ = session.createReceiver(address_pub);
            receiver_.setCapacity(queue_size);

    } catch(const std::exception& error) {
            std::cerr << error.what() << std::endl;
    }

    loop_time_ = node_handle.getLoopTime();

    // register thread for the subscriber
    boost::thread t2(&Subscriber::subscribe , this, f);
}

Subscriber::~Subscriber()
{
    shutdown();
}

void Subscriber::subscribe(boost::function< void(qpid::messaging::Message) > callbackFunc)
{
    while(!receiver_.isClosed()) {
        try {
            Message message;

            bool status = receiver_.fetch(message, Duration::SECOND * loop_time_);
            receiver_.getSession().acknowledge();

            if (status) {
                callbackFunc(message);
            }

        } catch(const std::exception& error) {
            std::cerr << error.what() << std::endl;
        }
    }
}

bool Subscriber::shutdown() {
    if (!receiver_.isClosed())
        receiver_.close();
    return true;
}
