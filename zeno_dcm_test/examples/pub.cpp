
#include <iostream>
#include <zeno_data_communication_manager/node.h>
#include <zeno_data_communication_manager/publisher.h>

#include <qpid/messaging/Message.h>
#include <qpid/types/Variant.h>

#include <iostream>

Message prepareMSG()
{
    Message message;
    
    Variant::Map content;
   
    Variant::List joint_names;
    joint_names.push_back(Variant("joint_1"));
    joint_names.push_back(Variant("joint_2"));
    joint_names.push_back(Variant("joint_3"));
    content["joint_names"] = joint_names;

    Variant::List joint_values;
    joint_values.push_back(Variant(0.0));
    joint_values.push_back(Variant(0.0));
    joint_values.push_back(Variant(0.0));
    content["joint_values"] = joint_values;

    encode(content, message);

    return message;
}

int main(int argc, char** argv) {

    double cycle_time = 1.0;
    
    std::string broker = "localhost:5672";

    std::string topic_type =  "amq.topic";

    std::string connectionOptions =  "{sasl_mechanisms:PLAIN,username:admin,password:admin}";

    DataCommunicationManager::Node node_handle;

    node_handle.initNode("joint_controller", broker, connectionOptions, true);

    DataCommunicationManager::Publisher pub(node_handle, "~joint_states", topic_type, 10);

    while(node_handle.isNodeRunning()) {
        pub.publish(prepareMSG());
        sleep(cycle_time);
    }

    return 0;
}
